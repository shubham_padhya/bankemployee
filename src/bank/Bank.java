/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package bank;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class Bank {

   private String name;
    
    
    
    public Bank(String name){
     this.name=name;   
        
    }
    
   public String getName(){ 
       
    return name;   
       
   }
    
   public void setName(String name){
       
    this.name=name;
   }
    
}

 class Employee {
    
    private String name;
    
    
    public Employee(String name){
        
        this.name=name;
        
        
    }
    
    
    public String getName(){
        
     return name;   
    }
    
    public void setName(String name){
     this.name=name;   
        
    }
    
    
}


 class Test {

public static void main(String [] args){
 
    
    Employee e1 = new Employee("sunil");
    Bank b1 = new Bank("RBI");
    
    
  System.out.println(b1.getName() + " " + e1.getName() );
  
    
    
}
    
}

    

